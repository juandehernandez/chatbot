import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.json.simple.JSONObject;

/**
 * Created by juandelacruz on 6/1/17.
 */

/**
 * Classe per a llegir tot el json de qualsevol webservice
 * i centrada en Bicing i l'arxiu local favorite_places.json
 */
public class Json {
    private static final String MOVIE_GENRES_WS = "https://api.themoviedb.org/3/genre/movie/list?language=es-ES&api_key=";
    private static final String MOVIE_ACTORS_WS = "https://api.themoviedb.org/3/person/popular?language=es-ES&api_key=";
    private static final String MOVIE_WS1 = "https://api.themoviedb.org/3/genre/";
    private static final String MOVIE_WS2 = "/movies?language=es-ES&include_adult=false&sort_by=created_at.asc&api_key=";
    private static final String MOVIE_WS3 = "https://api.themoviedb.org/3/person/";
    private static final String MOVIE_WS4 = "/movie_credits?language=es-ES&api_key=";
    private static final String API_KEY = "23d85844287bafb247c60c0436bd47e5";

    /**
     * Aqueta funció és de les més utilitzades ja que llegeix qualsevol json
     * @param url URL del webservice per a llegir, ja sigui de GoogleMaps o de Bicing.
     * @return JsonObject (Totes les dades visibles quan s'obra un webservice, tot el seu json general)
     * @throws IOException Si el BufferedReader és null, no s'ha obtingut res del webservice llançem excepció
     */
    public static JsonObject readJsonFromUrl(URL url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                return null;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String txt = readAll(br);
            JsonParser par = new JsonParser();
            JsonObject json = (JsonObject) par.parse(txt);

            return json;


        } finally {
            conn.disconnect();
        }
    }

    /**
     * Funció privada que s'executa a la funció anterior. Genera String a partir de BufferedReader
     * @param rd BufferedReader
     * @return String
     * @throws IOException Si el BufferedReader és null pot llançar IOException
     */
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    /**
     * Aquesta funció s'encarrega d'obtenir els géneres de películes, llegeix de la URL obtenint
     * un objecte JsonObject i les va posant a una llista
     * @return List<MovieGenre> Llista de géneres de películes
     */
    public static List<MovieGenre> getMovieGenres() {
        List<MovieGenre> movieGenres = new ArrayList<>();
        try {
            URL urlGenres = new URL(MOVIE_GENRES_WS+API_KEY);
            JsonObject obj = readJsonFromUrl(urlGenres);
            if (obj == null) {
                System.out.println("Finalitzem el programa degut a un error amb les películes");
                System.exit(0);
            }

            JsonArray genresJsonArray = obj.get("genres").getAsJsonArray();
            for (int i = 0; i < genresJsonArray.size(); i++) {
                MovieGenre movieGenre = new MovieGenre(genresJsonArray.get(i).getAsJsonObject().get("id").getAsInt(),
                        genresJsonArray.get(i).getAsJsonObject().get("name").getAsString());

                movieGenres.add(movieGenre);
            }
            return movieGenres;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<MovieActor> getMovieActors() {
        List<MovieActor> movieActors = new ArrayList<>();
        try {
            URL urlGenres = new URL(MOVIE_ACTORS_WS+API_KEY);
            JsonObject obj = readJsonFromUrl(urlGenres);
            if (obj == null) {
                System.out.println("Finalitzem el programa degut a un error amb les películes");
                System.exit(0);
            }

            JsonArray actorsJsonArray = obj.get("results").getAsJsonArray();
            for (int i = 0; i < actorsJsonArray.size(); i++) {
                String[] arr = actorsJsonArray.get(i).getAsJsonObject().get("name").getAsString().split(" ");
                String name = arr[0];
                String surname = arr[1];
                MovieActor movieActor = new MovieActor(name, surname, actorsJsonArray.get(i).getAsJsonObject().get("id").getAsInt());

                movieActors.add(movieActor);
            }
            return movieActors;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Movie> getMovieByGenre(int id) {
        List<Movie> movies = new ArrayList<>();

        try {
            URL urlMovies = new URL(MOVIE_WS1+id+MOVIE_WS2+API_KEY);
            JsonObject obj = readJsonFromUrl(urlMovies);
            if (obj == null) {
                System.out.println("Finalitzem el programa degut a un error amb les películes");
                System.exit(0);
            }

            JsonArray moviesJsonArray = obj.get("results").getAsJsonArray();

            for (int i = 0; i < moviesJsonArray.size(); i++) {
                String releaseDate = "No especificada";
                if (moviesJsonArray.get(i).getAsJsonObject().get("release_date") != null) {
                    releaseDate = moviesJsonArray.get(i).getAsJsonObject().get("release_date").getAsString();
                }

                String overview = "No especificada";
                if (!moviesJsonArray.get(i).getAsJsonObject().get("overview").isJsonNull()) {
                    overview = moviesJsonArray.get(i).getAsJsonObject().get("overview").getAsString();
                }

                Movie movie = new Movie(moviesJsonArray.get(i).getAsJsonObject().get("original_title").getAsString(),
                        overview,
                        releaseDate);
                movies.add(movie);
            }
            return movies;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<Movie> getMovieByActor(int id) {
        List<Movie> movies = new ArrayList<>();

        try {
            URL urlMovies = new URL(MOVIE_WS3+id+MOVIE_WS4+API_KEY);
            JsonObject obj = readJsonFromUrl(urlMovies);
            if (obj == null) {
                System.out.println("Finalitzem el programa degut a un error amb les películes");
                System.exit(0);
            }

            JsonArray moviesJsonArray = obj.get("cast").getAsJsonArray();

            for (int i = 0; i < moviesJsonArray.size(); i++) {
                String releaseDate = "No especificada";
                if (moviesJsonArray.get(i).getAsJsonObject().get("release_date") != null) {
                    releaseDate = moviesJsonArray.get(i).getAsJsonObject().get("release_date").getAsString();
                }

                String overview = "No especificada";
                if (!moviesJsonArray.get(i).getAsJsonObject().get("overview").isJsonNull()) {
                    overview = moviesJsonArray.get(i).getAsJsonObject().get("overview").getAsString();
                }

                Movie movie = new Movie(moviesJsonArray.get(i).getAsJsonObject().get("original_title").getAsString(),
                        overview,
                        releaseDate);
                movies.add(movie);
            }
            return movies;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

}