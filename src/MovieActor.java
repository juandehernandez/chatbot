public class MovieActor {
    private String name;
    private String surname;
    private int id;

    public MovieActor(String name, String surname, int id) {
        this.name = name;
        this.surname = surname;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getFullName() {
        return name + " " + surname;
    }

    public int getId() {
        return id;
    }
}
