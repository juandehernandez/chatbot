import com.mysql.jdbc.StringUtils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.Normalizer;
import java.util.List;

public class Controller implements ActionListener {
    private Chat chat;
    private Database database;
    private List<MovieGenre> movieGenreList;
    private int posList = 0;
    private List<MovieActor> movieActorList;
    private String valor = new String();
    private List<Movie> movieList = null;

    public Controller(Chat chat, List<MovieGenre> movieGenreList, List<MovieActor> movieActorList) {
        this.chat = chat;
        try {
            database = new Database("sensaci0_root", "skull123",
                    Database.DATABASE_NAME, "es42.siteground.eu", 3306);
        } catch(SQLException e) {
            e.printStackTrace();
        }
        this.movieGenreList = movieGenreList;
        this.movieActorList = movieActorList;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        String missatge = chat.obtenirMissatge();
        chat.afegeixMissatge(missatge, "me");
        chat.esborraMissatge();

        String[] arr = missatge.split(" ");

        if (Main.actual_context == 4 && Main.last_context == 4) {
            for ( String ss : arr) {
                for (int i = 0; i < movieGenreList.size(); i++) {
                    String choosedGenre = Normalizer.normalize(ss.toLowerCase(), Normalizer.Form.NFD);
                    choosedGenre = choosedGenre.replaceAll("[^\\p{ASCII}]", "");
                    String movieGenre = Normalizer.normalize(movieGenreList.get(i).getName().toLowerCase(), Normalizer.Form.NFD);
                    movieGenre = movieGenre.replaceAll("[^\\p{ASCII}]", "");
                    if (movieGenre.equals(choosedGenre)) {
                        movieList = Json.getMovieByGenre(movieGenreList.get(i).getId());
                        break;
                    }
                }
            }

            if (movieList != null) {
                if (posList < movieList.size()) {
                    valor = movieList.get(posList).toString();
                    insertBotMessage();
                    posList++;

                    return;
                }
            }

        } else if (Main.actual_context == 5 && Main.last_context == 5) {

            for ( String ss : arr) {
                for (int i = 0; i < movieActorList.size(); i++) {
                    String choosedActor = Normalizer.normalize(ss.toLowerCase(), Normalizer.Form.NFD);
                    choosedActor = choosedActor.replaceAll("[^\\p{ASCII}]", "");
                    String movieActor = Normalizer.normalize(movieActorList.get(i).getName().toLowerCase(), Normalizer.Form.NFD);
                    movieActor = movieActor.replaceAll("[^\\p{ASCII}]", "");
                    String movieActor2 = Normalizer.normalize(movieActorList.get(i).getSurname().toLowerCase(), Normalizer.Form.NFD);
                    movieActor2 = movieActor2.replaceAll("[^\\p{ASCII}]", "");
                    if (movieActor.equals(choosedActor) || movieActor2.equals(choosedActor)) {
                        movieList = Json.getMovieByActor(movieActorList.get(i).getId());
                        break;
                    }
                }
            }

            if (movieList != null) {
                if (posList < movieList.size()) {
                    valor = movieList.get(posList).toString();
                    insertBotMessage();
                    posList++;

                    return;
                }
            }
        }

        for ( String ss : arr) {

            valor = database.obtenirValor(ss);
            if (valor != null) {
                break;
            }
        }
        if (valor == null) {
            for ( String ss : arr) {

                valor = database.obtenirValorForaContext(ss);
                if (valor != null) {
                    break;
                }
            }
        }

        insertBotMessage();
    }

    private String genresToString() {
        String message = new String();
        for (int i = 0; i < movieGenreList.size() - 1; i++) {
            message = message + movieGenreList.get(i).getName() + ", ";
        }

        message = message + movieGenreList.get(movieGenreList.size() - 1).getName();

        return message;
    }

    private String actorsToString() {
        String message = new String();
        for (int i = 0; i < movieActorList.size() - 1; i++) {
            message = message + movieActorList.get(i).getFullName() + ", ";
        }

        message = message + movieActorList.get(movieActorList.size() - 1).getFullName();

        return message;
    }


    private void insertBotMessage () {

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        if (valor == null) {
                            chat.afegeixMissatge("No te entiendo", "bot");
                        } else {
                            chat.afegeixMissatge(valor, "bot");

                            if (Main.actual_context == 4 && Main.last_context != 4) {
                                chat.afegeixMissatge(genresToString(), "bot");
                                Main.last_context = Main.actual_context;
                                posList = 0;
                                movieList = null;

                            } else if (Main.actual_context == 5 && Main.last_context != 5) {
                                chat.afegeixMissatge(actorsToString(), "bot");
                                Main.last_context = Main.actual_context;
                                posList = 0;
                                movieList = null;

                            } else if (Main.actual_context == 4 && Main.last_context == 4
                                    || Main.actual_context == 5 && Main.last_context == 5) {
                                chat.afegeixMissatge(database.obtenirValorOutputContext(), "bot");

                            } else if (Main.actual_context == 20 || Main.actual_context == 21) {
                                chat.afegeixMissatge(movieList.get(posList).toString(), "bot");
                                posList++;
                                if (Main.actual_context == 20) {
                                    Main.actual_context = 14;
                                } else {
                                    Main.actual_context = 15;
                                }
                            } else if (Main.actual_context == 25 || Main.actual_context == 26) {
                                chat.afegeixMissatge(database.obtenirValorOutputContext(), "bot");
                            }

                            System.out.println("Actual context: " + Main.actual_context);
                            System.out.println("Last context: " + Main.last_context);
                        }
                    }
                },
                1500
        );
    }
}