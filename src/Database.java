/**
 * Created by juandelacruz on 13/6/17.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by juandelacruz on 18/5/17.
 */
public class Database {
    private Connection conn;
    private String connectionIp;

    public static final String DATABASE_NAME = "sensaci0_chatbot";

    public Database(String user, String password, String name, String ip, int port) throws SQLException {
        connectionIp = ip;

        String url = "jdbc:mysql://" + ip + ":" + port + "/" + name + "?autoReconnect=true&useSSL=false";

        //Aconseguim connexió a la base de dades que tenim en localhost

        DriverManager.setLoginTimeout(5);
        conn = DriverManager.getConnection(url, user, password);
    }

    public Connection getConn() {
        return conn;
    }

    public String obtenirValor(String clau) {
        try (Statement stmt = conn.createStatement()) {
            String sql;
            if (Main.actual_context == 1) {
                sql = "SELECT * FROM intent WHERE input_context = "+Main.actual_context;
            } else {
                sql = "SELECT * FROM intent WHERE k LIKE '% "+clau+" %' AND input_context = "+Main.actual_context;
            }
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    Main.last_context = Main.actual_context;
                    Main.actual_context = rs.getInt("id");
                    Main.output_context = rs.getInt("output_context");
                    return rs.getString("v");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String obtenirValorForaContext(String clau) {
        try (Statement stmt = conn.createStatement()) {

            String sql = "SELECT * FROM intent WHERE k LIKE '% "+clau+" %' ORDER BY id DESC";
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    Main.last_context = Main.actual_context;
                    Main.actual_context = rs.getInt("id");
                    Main.output_context = rs.getInt("output_context");
                    return rs.getString("v");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String obtenirValorOutputContext() {
        try (Statement stmt = conn.createStatement()) {
            String sql;
            sql = "SELECT * FROM intent WHERE id = "+Main.output_context;
            try (ResultSet rs = stmt.executeQuery(sql)) {
                while (rs.next()) {
                    Main.last_context = Main.actual_context;
                    Main.actual_context = rs.getInt("id");
                    Main.output_context = rs.getInt("output_context");
                    return rs.getString("v");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
