import javax.swing.*;
import java.util.List;

/**
 * Created by juandelacruz on 13/6/17.
 */
public class Main {
    public static int actual_context = 0;
    public static int last_context = 0;
    public static int output_context = 0;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            List<MovieGenre> movieGenreList = Json.getMovieGenres();
            List<MovieActor> movieActorList = Json.getMovieActors();

            Chat chat = new Chat();

            Controller controller = new Controller(chat, movieGenreList, movieActorList);

            chat.registraControlador(controller);

            chat.setVisible(true);
        });
    }
}