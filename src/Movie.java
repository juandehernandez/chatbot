public class Movie {
    private String title;
    private String description;
    private String releaseDate;

    public Movie(String title, String description, String releaseDate) {
        this.title = title;
        this.description = description;
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    @Override
    public String toString() {
        String movie;

        movie = "Título de la película: " + this.title + "\n";
        movie = movie + "Descripción: " + this.description + "\n";
        movie = movie + "Fecha de estreno: " + this.releaseDate + "\n";

        return movie;
    }
}
