import javax.swing.*;
import javax.swing.text.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Chat extends JFrame {
    JTextPane txt = new JTextPane();
    JScrollPane sp = new JScrollPane(txt);
    JTextField write = new JTextField();

    public Chat() {
        setTitle("Chat Bot");
        add(sp,BorderLayout.CENTER);
        add(write,BorderLayout.SOUTH);
        setSize(750,450);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void registraControlador(ActionListener actionListener){
        write.addActionListener(actionListener);
    }

    public void afegeixMissatge(String missatge, String autor) {
        if (autor.equals("me")) {
            appendToPane(txt, "Me: ", Color.BLUE);
        } else if (autor.equals("bot")) {
            appendToPane(txt, "Bot: ", Color.GREEN);
        }
        appendToPane(txt, missatge+"\n", Color.BLACK);
    }

    public String obtenirMissatge() {
        return write.getText();
    }

    public void esborraMissatge() {
        write.setText("");
    }

    private void appendToPane(JTextPane tp, String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

        aset = sc.addAttribute(aset, StyleConstants.FontFamily, "Lucida Console");
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = tp.getDocument().getLength();
        tp.setCaretPosition(len);
        tp.setCharacterAttributes(aset, false);
        tp.replaceSelection(msg);
    }
}